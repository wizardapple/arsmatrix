import React from 'react'
import './App.css'
import { BrowserRouter as Router, Route } from "react-router-dom"

import Navbar from './components/Navbar'
import Welcome from './views/Welcome'
import AuthCallback from './views/AuthCallback'
import Dashboard from './views/Dashboard'
import Login from './views/Login'
import Logout from './views/Logout';

const App = () => {
	return (
		<React.Fragment>
			<Router>
				<Navbar />
				<Route exact path="/" component={Welcome} />
				<Route path="/callback" component={AuthCallback} />
				<Route path="/dashboard" component={Dashboard} />
				<Route path="/login" component={Login} />
				<Route path="/logout" component={Logout} />
			</Router>
		</React.Fragment>
	)
}

export default App;
