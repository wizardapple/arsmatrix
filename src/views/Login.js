import React, { Component } from 'react'
import Auth from '../state/Auth'

class Login extends Component {
	render() {
		var auth = new Auth()
		auth.login()
		return (
			<React.Fragment>
				<br />
				<h2 className="subtitle has-text-centered">Loading...</h2>
			</React.Fragment>
		)
	}
}

export default Login