import React, { Component } from 'react'
import Auth from '../state/Auth'

class Logout extends Component {
	render() {
		var auth = new Auth()
		auth.logout()
		return (
			<React.Fragment>
				<br />
				<h2 className="subtitle has-text-centered">Logging you out...</h2>
			</React.Fragment>
		)
	}
}

export default Logout