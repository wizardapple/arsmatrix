import React from 'react'

const Welcome = () => (
	<React.Fragment>
		<section className="hero is-dark is-medium">
			<div className="hero-body">
				<div className="container">
					<h1 className="title has-text-centered">
						Welcome to ARSMatrix
     			 </h1>
					<h2 className="subtitle has-text-centered">
						The place where graduating seniors mentor rising seniors with the college application process.
				</h2>
				</div>
			</div>
		</section>
		<section className="section">
			<h1 className="title has-text-centered">How does ARSMatrix work?</h1>
			<div className="container">
				TODO: write description
			</div>
		</section>
	</React.Fragment>
)

export default Welcome