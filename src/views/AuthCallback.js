import React, { Component } from 'react'
import store from '../state/store'

class AuthCallback extends Component {
	componentDidMount() {
		store.storeAuthData(this.props.location.hash)
		this.props.history.push('/dashboard')
	}
	render() {
		return (
			<React.Fragment>
				<br />
				<h2 className="subtitle has-text-centered">Loading...</h2>
			</React.Fragment>
		)
	}
}

export default AuthCallback