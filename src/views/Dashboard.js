import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Redirect } from 'react-router-dom'
import classNames from 'classnames'
import store from '../state/store'
import mappings from '../vectorMappings'

import StatusCard from '../components/StatusCard'

class Dashboard extends Component {
	componentWillMount() {
		store.getMatchData()
	}
	generateRaceDescription() {
		var raceDescription = []
		if (this.props.store.matchData.race_white) { raceDescription.push("White") }
		if (this.props.store.matchData.race_african_american) { raceDescription.push("African-American") }
		if (this.props.store.matchData.race_american_indian) { raceDescription.push("American Indian") }
		if (this.props.store.matchData.race_asian) { raceDescription.push("Asian") }
		if (this.props.store.matchData.race_asian_indian) { raceDescription.push("Asian Indian") }
		if (this.props.store.matchData.race_chinese) { raceDescription.push("Chinese") }
		if (this.props.store.matchData.race_filipino) { raceDescription.push("Filipino") }
		if (this.props.store.matchData.race_japanese) { raceDescription.push("Japansese") }
		if (this.props.store.matchData.race_korean) { raceDescription.push("Korean") }
		if (this.props.store.matchData.race_vietnamese) { raceDescription.push("Vietnamese") }
		if (this.props.store.matchData.race_native_hawaiian) { raceDescription.push("Native Hawaiian") }
		if (this.props.store.matchData.race_middle_eastern) { raceDescription.push("Middle Eastern") }
		if (this.props.store.matchData.race_guam_chamorro) { raceDescription.push("Guam/Chamorro") }
		if (this.props.store.matchData.race_samoan) { raceDescription.push("Samoan") }
		if (this.props.store.matchData.race_other_pi) { raceDescription.push("Other Pacific Islander") }
		if (this.props.store.matchData.race_hispanic) { raceDescription.push("Hispanic") }
		if (this.props.store.matchData.race_mexican_american) { raceDescription.push("Mexican-American") }
		if (this.props.store.matchData.race_puerto_rican) { raceDescription.push("Puerto Rican") }
		if (this.props.store.matchData.race_cuban) { raceDescription.push("Cuban") }
		return raceDescription.join(', ')
	}
	render() {
		if (!this.props.store.isLoggedIn) {
			return (
				<Redirect to="/login" />
			)
		}
		var cardClassNames = "box"
		if (this.props.store.isLoadingUser) {
			cardClassNames += " is-hidden"
		}
		var intlColumn = null
		if (this.props.store.matchData.intl_region !== 0) {
			intlColumn = (<StatusCard header="International Region" text={mappings.intlRegionMappings[this.props.store.matchData.intl_region]} />)
		}
		return (
			<section className="section">
				<h1 className="title">Welcome, {this.props.store.userData.name}!</h1>
				<h2 className="subtitle">
					{this.props.store.matchData.matchConfirmed ? 'You have been matched.' : 'You haven\'t been matched yet.'}
				</h2>
				<br />
				<h3 className="title is-5">Your Matching Profile</h3>
				<div className="container is-fluid-mobile">
					<div className={cardClassNames}>
						<article className="media">
							<div className="media-left">
								<figure className="image is-64x64">
									<img src={this.props.store.userData.picture} alt="profile" />
								</figure>
							</div>
							<div className="media-content">
								<div className="content">
									<h4 className="title is-4">{this.props.store.matchData.username || "Loading..."}</h4>
									<h5 className="subtitle">{mappings.residencyText[this.props.store.matchData.residency - 1] || ""} {this.props.store.matchData.grade ? (this.props.store.matchData.grade === 12 ? "Graduating Senior/Undergrad" : "Rising Senior") : ""}</h5>
									<strong>{mappings.adviceTypes[this.props.store.matchData.field_proximity] || "Loading"} pairing preference</strong>
									<div className="columns">
										<StatusCard header="Major" text={mappings.majorMappings[this.props.store.matchData.major] || " "} />
										<StatusCard header="General Area" text={mappings.generalAreas[this.props.store.matchData.gen_area] || " "} />
										<StatusCard header="Available" text={mappings.availabilityMappings[this.props.store.matchData.availability]} />
									</div>
									<div className="columns">
										<StatusCard header="Gender" text={mappings.genderMappings[this.props.store.matchData.gender]} />
										<StatusCard header="Gender Identity" text={mappings.genderIDMappings[this.props.store.matchData.gender_id]} />
										<StatusCard header="Financial Aid" text={mappings.finaidMappings[this.props.store.matchData.finaid]} />
									</div>
									<div className="columns">
										{intlColumn}
										<StatusCard header="School System" text={mappings.schoolSystemMappings[this.props.store.matchData.school_sys]} />
										<StatusCard header="Race" text={this.generateRaceDescription()} />
									</div>
									<div className="level is-mobile">
										<div className="level-left">
											<span className={classNames("level-item", { "is-hidden": this.props.store.matchData.sm_discord !== 1 })}>
												<span className="icon is-large">
													<i className="subtitle is-3 fab fa-discord" aria-hidden="true"></i>
												</span>
											</span>
											<span className={classNames("level-item", { "is-hidden": this.props.store.matchData.sm_insta !== 1 })}>
												<span className="icon is-large">
													<i className="subtitle is-3 fab fa-instagram" aria-hidden="true"></i>
												</span>
											</span>
											<span className={classNames("level-item", { "is-hidden": this.props.store.matchData.sm_reddit !== 1 })}>
												<span className="icon is-large">
													<i className="subtitle is-3 fab fa-reddit" aria-hidden="true"></i>
												</span>
											</span>
											<span className={classNames("level-item", { "is-hidden": this.props.store.matchData.sm_skype !== 1 })}>
												<span className="icon is-large">
													<i className="subtitle is-3 fab fa-skype" aria-hidden="true"></i>
												</span>
											</span>
											<span className={classNames("level-item", { "is-hidden": this.props.store.matchData.sm_whatsapp !== 1 })}>
												<span className="icon is-large">
													<i className="subtitle is-3 fab fa-whatsapp" aria-hidden="true"></i>
												</span>
											</span>
											<span className={classNames("level-item", { "is-hidden": this.props.store.matchData.sm_email !== 1 })}>
												<span className="icon is-large">
													<i className="subtitle is-3 fa fa-envelope" aria-hidden="true"></i>
												</span>
											</span>
											<span className={classNames("level-item", { "is-hidden": this.props.store.matchData.sm_cellphone !== 1 })}>
												<span className="icon is-large">
													<i className="subtitle is-3 fa fa-phone" aria-hidden="true"></i>
												</span>
											</span>
										</div>
									</div>
								</div>
							</div>
						</article>
					</div>
				</div>
			</section>
		)
	}
}

export default inject('store')(observer(Dashboard))