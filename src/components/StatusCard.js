import React from 'react'

const StatusCard = props => (
	<div className="column">
		<strong>{props.header}</strong>: {props.text}
	</div>
)

export default StatusCard