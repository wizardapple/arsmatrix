import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { inject, observer } from 'mobx-react'

class Navbar extends Component {
	render() {
		var buttons = null
		if (!this.props.store.isLoggedIn) {
			buttons = (
				<div className="navbar-item">
					<div className="buttons">
						<Link to="/login" className="button is-light">Login</Link>
						<Link to="/signup" className="button is-link">Sign up</Link>
					</div>
				</div>
			)
		} else {
			buttons = (
				<div className="navbar-item has-dropdown is-hoverable">
					<span className="navbar-link">
						{this.props.store.userData.name || 'You'}
					</span>
					<div className="navbar-dropdown">
						<Link to="/profile" className="navbar-item">Profile</Link>
						<hr className="navbar-divider" />
						<Link to="/logout" className="navbar-item">Log out</Link>
					</div>
				</div>
			)
		}
		return (
			<nav className="navbar is-dark" role="navigation" aria-label="main navigation">
				<div className="navbar-brand">
					<Link to="/" className="navbar-item">
						<strong>ARSMatrix</strong><sub>(&beta;)</sub>
					</Link>
				</div>
				<div className="navbar-menu is-active">
					<div className="navbar-start">
						<Link to="/" className="navbar-item">Home</Link>
						<Link to="/dashboard" className="navbar-item">Dashboard</Link>
					</div>
					<div className="navbar-end">
						{buttons}
					</div>
				</div>
			</nav>
		)
	}
}
export default inject('store')(observer(Navbar))