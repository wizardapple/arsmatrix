import auth0 from 'auth0-js'
import authConstants from '../authConstants'

export default class Auth {
	auth0 = new auth0.WebAuth({
		domain: authConstants.authDomain,
		clientID: authConstants.clientID,
		redirectUri: `${authConstants.appDomain}/callback`,
		responseType: 'token id_token',
		audience: 'https://localhost:8000',
		scope: 'openid profile read:matchdata'
	})

	login() {
		this.auth0.authorize()
	}
	logout() {
		this.auth0.logout({ returnTo: authConstants.appDomain })
	}
	getUserProfile(token) {
		return new Promise((resolve, reject) => {
			this.auth0.client.userInfo(token, (err, profile) => {
				if (err) reject(err)
				resolve(profile)
			})
		})
	}
}