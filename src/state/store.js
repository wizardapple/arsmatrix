import { observable, decorate, action } from 'mobx'
import Auth from '../state/Auth'
import authConstants from '../authConstants'

class Store {
	userData = {}
	authData = {}
	matchData = {}
	isLoggedIn = false
	isLoadingUser = false
	storeAuthData(queryStr) {
		queryStr = queryStr.replace('#', '?')
		const params = new URLSearchParams(queryStr)
		this.authData.accessToken = params.get('access_token')
		this.authData.expires = params.get('expires_in')
		this.authData.tokenType = params.get('token_type')
		this.authData.state = params.get('state')
		this.authData.idToken = params.get('id_token')
		this.isLoggedIn = true
		this.getProfile()
	}
	getProfile() {
		var a = new Auth()
		a.getUserProfile(this.authData.accessToken).then(profile => {
			this.userData = profile
		}).catch(e => console.error(e))
	}
	getMatchData() {
		this.isLoadingUser = true
		if (!this.userData.sub) {
			setTimeout(this.getMatchData.bind(this), 1000)
		} else {
			let headers = new Headers({
				authorization: `Bearer ${this.authData.accessToken}`
			})
			let settings = {
				method: 'GET',
				headers: headers,
				mode: 'cors',
				cache: 'default'
			}
			let req = new Request(`${authConstants.backendDomain}/user/${this.userData.sub}/profile`, settings)
			fetch(req)
				.then(res => res.json())
				.then(json => this.matchData = json)
				.then(() => { this.isLoadingUser = false })
		}
	}
}

decorate(Store, {
	userData: observable,
	authData: observable,
	isLoggedIn: observable,
	isLoadingUser: observable,
	matchData: observable,
	storeAuthData: action,
	getProfile: action,
	getMatchData: action
})

const dataStore = new Store()

export default dataStore;