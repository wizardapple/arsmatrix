var vectorMappings = {}

vectorMappings.residencyText = [
	"American",
	"Canadian",
	"International"
]
vectorMappings.adviceTypes = {
	1: "Specialized",
	20: "General Area",
	300: "Anyone"
}

vectorMappings.majorMappings = {
	100: "Agriculture & Forestry",
	107: "Food Science",
	121: "Architecture & Design",
	123: "Town & Country Planning and Landscape Design",
	140: "American Studies",
	142: "Middle Eastern & African Studies",
	161: "Biological Sciences",
	166: "Anatomy & Physiology",
	200: "Land & Property Management",
	201: "Accounting & Finance",
	211: "Hospitality",
	218: "Marketing",
	252: "Communication & Media Studies",
	254: "Journalism",
	303: "Computer Science",
	390: "Building",
	400: "Education",
	433: "Counselling",
	450: "Engineering (General)",
	451: "Aeronautical & Manufacturing Engineering",
	454: "Biomedical Engineering",
	455: "Chemical Engineering",
	456: "Civil Engineering",
	459: "Electrical & Electronic Engineering",
	466: "Materials Technology",
	467: "Mechanical Engineering",
	516: "Robotics",
	520: "Literature",
	522: "Creative Writing",
	550: "Linguistics",
	556: "French",
	557: "German",
	558: "Italian",
	560: "Russian & East European Languages",
	603: "Sports Science",
	617: "Medical Technology",
	619: "Nursing",
	627: "Dentistry",
	628: "Medicine",
	629: "Pharmacology & Pharmacy",
	630: "Veterinary Medicine",
	678: "Occupational Therapy",
	680: "Ophthalmology & Orthoptics",
	682: "Physiotherapy",
	700: "History",
	702: "Classics & Ancient History",
	712: "Law",
	730: "Librarianship & Information Management",
	742: "Mathematics",
	800: "Leisure",
	802: "Recreation & Tourism",
	821: "Philosophy",
	822: "Theology & Religious Studies",
	833: "Physics and Astronomy",
	836: "Chemistry",
	837: "Geology",
	870: "Psychology",
	883: "Social Policy",
	884: "Youth Work",
	890: "Forensic Science",
	901: "Anthropology",
	902: "Archaeology",
	903: "Criminology",
	904: "Economics",
	906: "International Relations",
	907: "Politics",
	908: "Sociology",
	941: "History of Art",
	942: "Dance & Cinematics",
	943: "Drama, Dance & Cinematics",
	944: "Art & Design",
	945: "Fashion",
	946: "Film Making",
	950: "Music Studies (Composition, history, etc)",
	952: "Music Performance",
	999: "Undecided"
}

vectorMappings.generalAreas = {
	100: "Agriculture",
	200: "Business",
	300: "Technology",
	390: "Trades and Personal Services",
	450: "Engineering",
	600: "Health and Medicine",
	710: "Law",
	720: "Liberal Arts",
	770: "Multi-/Interdisciplinary Studies",
	830: "Sciences",
	880: "Public and Social Services",
	900: "Social Sciences",
	940: "Performing Arts",
	999: "Undecided"
}

vectorMappings.availabilityMappings = {
	1: "Rarey",
	2: "Occasionally",
	3: "Sometimes",
	4: "Mostly",
	5: "Almost always"
}

vectorMappings.genderMappings = {
	1: "Male",
	2: "Female",
	3: "Transgender",
	4: "Gender neutral",
	5: "Other"
}

vectorMappings.genderIDMappings = {
	1: "Straight",
	5: "Gay/Lesbian",
	6: "Bisexual",
	7: "Asexual",
	8: "Other"
}

vectorMappings.finaidMappings = {
	1: "Applied/Will Apply",
	2: "Did Not Apply/Will Not Apply",
	3: "Might Apply"
}

vectorMappings.intlRegionMappings = {
	200: "Mexico",
	240: "Central America",
	250: "Caribbean",
	270: "South America",
	300: "Eastern Africa",
	310: "Middle Africa (Congo)",
	320: "Northern Africa",
	330: "Southern Africa",
	340: "Western Africa (Guinea)",
	400: "Middle East",
	500: "Eastern Europe",
	510: "Northern Europe",
	520: "Southern Europe",
	530: "Western Europe",
	600: "Southeast Asia",
	610: "East Asia",
	620: "South Asia",
	630: "Central Asia",
	640: "Western Asia",
	700: "Australia and New Zealand",
	800: "Melanesia",
	830: "Micronesia",
	860: "Polynesia"
}

vectorMappings.schoolSystemMappings = {
	1: "AP",
	2: "IB",
	3: "A-Levels",
	4: "IG",
	5: "ECF",
	6: "100 Point Scale",
	7: "GPA",
	8: "Other",
}

export default vectorMappings